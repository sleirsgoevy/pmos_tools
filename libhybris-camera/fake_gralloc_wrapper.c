#include <stdint.h>

typedef void* buffer_handle_t;
struct Rect;
struct YCbCrLayout;

void _ZN7android14GrallocWrapper6MapperC1Ev(void* self){}
void _ZN7android14GrallocWrapper6MapperC2Ev(void* self){}
void _ZN7android14GrallocWrapper9AllocatorC1ERKNS0_6MapperE(void* self){}
void _ZN7android14GrallocWrapper9AllocatorC2ERKNS0_6MapperE(void* self){}

void _ZNK7android14GrallocWrapper6Mapper10freeBufferEPK13native_handle(void* self, buffer_handle_t handle)
{
    __builtin_trap();
}

void _ZNK7android14GrallocWrapper6Mapper12importBufferERKNS_8hardware11hidl_handleEPPK13native_handle(void* self, void* src, buffer_handle_t* dst)
{
    __builtin_trap();
}

void _ZNK7android14GrallocWrapper6Mapper16createDescriptorERKNS_8hardware8graphics6mapper4V2_07IMapper20BufferDescriptorInfoEPNS2_8hidl_vecIjEE(void* self, void* src, void* dst)
{
    __builtin_trap();
}

static int(*lock_simple)(buffer_handle_t handle, uint64_t usage, const struct Rect* bounds, int acquireFence, void** mapping);
static int(*lock_ycbcr)(buffer_handle_t handle, uint64_t usage, const struct Rect* bounds, int acquireFence, struct YCbCrLayout* ycbcr);
static int(*unlock)(buffer_handle_t handle);

void set_lock_unlock_ycbcr(void* fn1, void* fn2, void* fn3)
{
    lock_simple = fn1;
    lock_ycbcr = fn2;
    unlock = fn3;
}

int _ZNK7android14GrallocWrapper6Mapper4lockEPK13native_handleyRKNS_8hardware8graphics6mapper4V2_07IMapper4RectEiPNS8_11YCbCrLayoutE(void* self, buffer_handle_t handle, uint64_t usage, const struct Rect* bounds, int acquireFence, struct YCbCrLayout* ycbcr)
{
    return lock_ycbcr(handle, usage, bounds, acquireFence, ycbcr);
}

int _ZNK7android14GrallocWrapper6Mapper4lockEPK13native_handleyRKNS_8hardware8graphics6mapper4V2_07IMapper4RectEiPPv(void* self, buffer_handle_t handle, uint64_t usage, const struct Rect* bounds, int acquireFence, void** mapping)
{
    return lock_simple(handle, usage, bounds, acquireFence, mapping);
}

void _ZNK7android14GrallocWrapper6Mapper6unlockEPK13native_handle(void* self, buffer_handle_t handle)
{
    unlock(handle);
}

void _ZNK7android14GrallocWrapper9Allocator13dumpDebugInfoEv(void* self)
{
    __builtin_trap();
}

void _ZNK7android14GrallocWrapper9Allocator8allocateENS_8hardware8hidl_vecIjEEjPjPPK13native_handle(void* self)
{
    __builtin_trap();
}
