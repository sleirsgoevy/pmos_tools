#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <inttypes.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <poll.h>

#ifdef __ANDROID__

#include <dlfcn.h>
#define android_dlopen dlopen
#define android_dlsym dlsym

#else

void* android_dlopen(const char*, int);
void* android_dlsym(void*, const char*);

#endif

#define HARDWARE_MODULE_TAG 0x48574d54 //'HWMT'
#define HARDWARE_DEVICE_TAG 0x48574454 //'HWDT'

struct camera_metadata_entry
{
    size_t index;
    uint32_t tag;
    uint8_t type;
    size_t count;
    union
    {
        uint8_t* u8;
        int32_t* i32;
        float* f;
        int64_t* i64;
        double* d;
        //camera_metadata_rational_t* r;
    } data;
};

enum
{
    TYPE_BYTE,
    TYPE_INT32,
    TYPE_FLOAT,
    TYPE_INT64,
    TYPE_DOUBLE,
    TYPE_RATIONAL,
};

struct camera_info
{
    int facing;
    int orientation;
    uint32_t device_version;
    const struct camera_metadata_t* static_camera_characteristics;
    int resource_cost;
    char** conflicting_devices;
    size_t conflicting_devices_length;
};

struct hw_device_t
{
    uint32_t tag;
    uint32_t version;
    struct hw_module_t* module;
    uintptr_t reserved[12];
    int(*close)(struct hw_device_t* dev);
};

struct camera_device
{
    struct hw_device_t common;
    struct camera_device_ops* ops;
    void* priv;
};

struct camera_device_ops
{
    void* set_preview_window; //TODO
    void* set_callbacks; //TODO
    void(*enable_msg_type)(struct camera_device*, int32_t);
    void(*disable_msg_type)(struct camera_device*, int32_t);
    int(*msg_type_enabled)(struct camera_device*, int32_t);
    int(*start_preview)(struct camera_device*);
    void(*stop_preview)(struct camera_device*);
    int(*preview_enabled)(struct camera_device*);
    int(*store_meta_data_in_buffers)(struct camera_device*, int);
    int(*start_recording)(struct camera_device*);
    void(*stop_recording)(struct camera_device*);
    int(*recording_enabled)(struct camera_device*);
    void(*release_recording_frame)(struct camera_device*, const void*);
    int(*auto_focus)(struct camera_device*);
    int(*cancel_auto_focus)(struct camera_device*);
    int(*take_picture)(struct camera_device*);
    int(*cancel_picture)(struct camera_device*);
    int(*set_parameters)(struct camera_device*, const char*);
    char*(*get_parameters)(struct camera_device*);
    void(*put_parameters)(struct camera_device*, char*);
    int(*send_command)(struct camera_device*, int32_t, int32_t, int32_t);
    void(*release)(struct camera_device*);
    int(*dump)(struct camera_device*, int fd);
};

struct camera3_device
{
    struct hw_device_t common;
    struct camera3_device_ops* ops;
    void* priv;
};

struct camera3_capture_result;
struct camera3_notify_msg;

struct camera3_callback_ops
{
    void(*process_capture_result)(const struct camera3_callback_ops*, const struct camera3_capture_result*);
    void(*notify)(const struct camera3_callback_ops*, const struct camera3_notify_msg*);
};

struct camera3_stream
{
    int stream_type;
#if 1 //google headers
    uint32_t width;
    uint32_t height;
    int format;
    uint32_t usage;
    uint32_t max_buffers;
#else //debug prints imply another order, but they are wrong
    uint32_t height;
    uint32_t max_buffers;
    uint32_t width;
    uint32_t usage;
    int format;
#endif
    void* priv;
    uint32_t /*android_dataspace_t*/ data_space;
    int rotation;
    void* reserved[7];
};

enum
{
    CAMERA3_STREAM_OUTPUT = 0,
};

struct camera3_stream_configuration
{
    uint32_t num_streams;
    struct camera3_stream** streams;
    uint32_t operation_mode;
};

enum
{
    CAMERA3_STREAM_CONFIGURATION_NORMAL_MODE = 0,
};

enum
{
    CAMERA3_BUFFER_STATUS_OK = 0,
};

typedef void* buffer_handle_t;

struct camera3_stream_buffer
{
    struct camera3_stream* stream;
    buffer_handle_t* buffer;
    int status;
    int acquire_fence;
    int release_fence;
};

struct camera3_stream_buffer_set;

struct camera3_capture_request
{
    uint32_t frame_number;
    const struct camera_metadata_t* settings;
    struct camera3_stream_buffer* input_buffer;
    uint32_t num_output_buffers;
    struct camera3_stream_buffer* output_buffers;
    uint32_t num_physical_settings;
    const char** physcam_id;
    const struct camera_metadata_t** physcam_settings;
};

struct camera3_device_ops
{
    int(*initialize)(const struct camera3_device*, const struct camera3_callback_ops*);
    int(*configure_streams)(const struct camera3_device*, const struct camera3_stream_configuration*);
    int(*register_stream_buffers)(const struct camera3_device*, const struct camera3_stream_buffer_set*);
    const struct camera_metadata_t*(*construct_default_request_settings)(const struct camera3_device*, int);
    int(*process_capture_request)(const struct camera3_device*, const struct camera3_capture_request*);
    void(*get_metadata_vendor_tag_ops)(const struct camera3_device*, void* /* vendor_tag_query_ops_t* */);
    void(*dump)(const struct camera3_device*, int);
    int(*flush)(const struct camera3_device*);
    void* reserved[8];
};

struct camera3_jpeg_blob
{
    uint16_t jpeg_blob_id;
    uint32_t jpeg_size;
};

enum
{
    CAMERA3_JPEG_BLOB_ID = 255,
};

struct hw_module_t
{
    union
    {
        struct
        {
            uint32_t tag;
            uint16_t module_api_version;
            uint16_t hal_api_version;
            const char* id;
            const char* name;
            const char* author;
            struct hw_module_methods_t* methods;
            void* dso;
        };
        char padding[128];
    };
};

struct hw_module_methods_t
{
    int(*open)(const struct hw_module_t*, const char*, struct hw_device_t**);
};

struct camera_module_callbacks
{
    void(*camera_device_status_change)(const struct camera_module_callbacks*, int camera_id, int new_status);
    void(*torch_mode_status_change)(const struct camera_module_callbacks*, const char* camera_id, int new_status);
};

struct camera_module
{
    struct hw_module_t common;
    int(*get_number_of_cameras)(void);
    int(*get_camera_info)(int, struct camera_info*);
    int(*set_callbacks)(const struct camera_module_callbacks*);
};

struct alloc_device_t
{
    struct hw_device_t common;
    int(*alloc)(struct alloc_device_t*, int width, int height, int format, int usage, buffer_handle_t* handle, int* stride);
    int(*free)(struct alloc_device_t*, buffer_handle_t*);
};

struct gralloc1_device
{
    struct hw_device_t common;
    void(*getCapabilities)(struct gralloc1_device*, uint32_t* count, int32_t* out);
    void*(*getFunction)(struct gralloc1_device*, int32_t descriptor);
};

typedef uint64_t gralloc1_buffer_descriptor_t;

enum
{
    //http://web.archive.org/web/20231203165708/https://webcache.googleusercontent.com/search?q=cache:https:%2F%2Ffossies.org%2Flinux%2Fmisc%2Fmesa-23.3.0-rc1.tar.xz%2Fmesa-23.3.0-rc1%2Finclude%2Fandroid_stub%2Fhardware%2Fgralloc1.h
    //IDK why it differs from google headers
    //if you get a crash/error in gralloc-related functions, maybe you need to change this
    GRALLOC1_FUNCTION_DUMP = 1, //not present in google headers
    GRALLOC1_FUNCTION_CREATE_DESCRIPTOR = 2,
    GRALLOC1_FUNCTION_DESTROY_DESCRIPTOR = 3,
    GRALLOC1_FUNCTION_SET_CONSUMER_USAGE = 4,
    GRALLOC1_FUNCTION_SET_DIMENSIONS = 5,
    GRALLOC1_FUNCTION_SET_FORMAT = 6,
    GRALLOC1_FUNCTION_SET_PRODUCER_USAGE = 7,
    GRALLOC1_FUNCTION_ALLOCATE = 14,
    GRALLOC1_FUNCTION_RELEASE = 16,
    GRALLOC1_FUNCTION_GET_NUM_FLEX_PLANES = 17,
    GRALLOC1_FUNCTION_LOCK = 18,
    GRALLOC1_FUNCTION_LOCK_FLEX = 19,
    GRALLOC1_FUNCTION_UNLOCK = 20,
};

enum
{
    HAL_PIXEL_FORMAT_BLOB = 0x21,
    HAL_PIXEL_FORMAT_YCBCR_420_888 = 0x23,
};

enum
{
    GRALLOC_USAGE_SW_READ_OFTEN = 3,
    GRALLOC_USAGE_SW_WRITE_NEVER = 0,
    GRALLOC_USAGE_HW_CAMERA_WRITE = 0x20000,
};

enum
{
    GRALLOC1_CONSUMER_USAGE_CPU_READ_OFTEN = 6,
    GRALLOC1_PRODUCER_USAGE_CAMERA = 131072,
};

static void* try_dlopen(const char* first, ...)
{
    size_t l = strlen(first);
    va_list ls;
    va_start(ls, first);
    for(const char* cur; (cur = va_arg(ls, const char*));)
        l += strlen(cur);
    va_end(ls);
    char* buf = malloc(l+1);
    char* p = buf;
    l = strlen(first);
    memcpy(p, first, l);
    p += l;
    va_start(ls, first);
    for(const char* cur; (cur = va_arg(ls, const char*));)
    {
        l = strlen(cur);
        memcpy(p, cur, l);
        p += l;
    }
    va_end(ls);
    *p = 0;
    void* handle = android_dlopen(buf, 2);
    free(buf);
    return handle;
}

static struct hw_module_t* open_module(const char* name, const char* soc)
{
    void* handle;
    if(soc && ((handle = try_dlopen("/vendor/lib/hw/", name, ".", soc, ".so", NULL))
            || (handle = try_dlopen("/system/lib/hw/", name, ".", soc, ".so", NULL))
            || (handle = try_dlopen("/odm/lib/hw/", name, ".", soc, ".so", NULL))))
    {
        struct hw_module_t* ans = android_dlsym(handle, "HMI");
        if(!ans)
        {
            fprintf(stderr, "Failed to resolve HMI from %s.%s\n", name, soc);
            exit(1);
        }
        if(ans->tag != HARDWARE_MODULE_TAG)
        {
            fprintf(stderr, "Invalid module tag 0x%x\n", ans->tag);
            exit(1);
        }
        return ans;
    }
    //fall back to libhardware lookup
    handle = android_dlopen("libhardware.so", 2);
    if(!handle)
    {
        fprintf(stderr, "Cannot open libhardware.so\n");
        exit(1);
    }
    int(*hw_get_module)(const char*, struct hw_module_t**) = android_dlsym(handle, "hw_get_module");
    if(!hw_get_module)
    {
        fprintf(stderr, "Failed to resolve hw_get_module\n");
        exit(1);
    }
    struct hw_module_t* module;
    int error = hw_get_module(name, &module);
    if(error)
    {
        fprintf(stderr, "Failed to get gralloc module: %d\n", error);
        exit(1);
    }
    if(module->tag != HARDWARE_MODULE_TAG)
    {
        fprintf(stderr, "Invalid module tag 0x%x\n", module->tag);
        exit(1);
    }
    return module;
}

struct Rect
{
    int left;
    int top;
    int width;
    int height;
};

static struct hw_device_t* the_gralloc;

enum android_flex_component
{
    FLEX_COMPONENT_Y = 1,
    FLEX_COMPONENT_Cb = 2,
    FLEX_COMPONENT_Cr = 4,
};

enum android_flex_format
{
    FLEX_FORMAT_YCbCr = 7,
};

struct android_flex_plane
{
    uint8_t* top_left;
    enum android_flex_component component;
    int32_t bits_per_component;
    int32_t bits_used;
    int32_t h_increment;
    int32_t v_increment;
    int32_t h_supersampling;
    int32_t v_supersampling;
};

struct android_flex_layout
{
    enum android_flex_format format;
    uint32_t num_planes;
    struct android_flex_plane* planes;
};

struct gralloc1_rect;

static struct android_flex_plane* get_flex_plane(struct android_flex_plane* data, size_t n, int which)
{
    for(size_t i = 0; i < n; i++)
        if(data[i].component == which)
            return data + i;
    return NULL;
}

struct YCbCrLayout
{
    uint8_t* y;
    uint8_t* cb;
    uint8_t* cr;
    uint32_t yStride;
    uint32_t cStride;
    uint32_t chromaStep;
};

//for brevity, i'm not implementing gralloc0 in these functions
//it can be trivially added if necessary

static int gralloc_lock_simple(buffer_handle_t handle, uint64_t usage, const struct Rect* bounds, int acquire_fence, void** mapping)
{
    if(the_gralloc->module->module_api_version < 0x0100)
    {
        fprintf(stderr, "gralloc0 is not supported\n");
        exit(1);
    }
    struct gralloc1_device* dev = (void*)the_gralloc;
    int32_t(*lock)(struct gralloc1_device*, buffer_handle_t, uint64_t, uint64_t, const struct gralloc1_rect*, void**, int32_t) = dev->getFunction(dev, GRALLOC1_FUNCTION_LOCK);
    if(!lock)
    {
        fprintf(stderr, "failed to resolve lock\n");
        exit(1);
    }
    int error = lock(dev, handle, usage, usage, (void*)bounds, mapping, acquire_fence);
    if(error)
    {
        fprintf(stderr, "failed to lock buffer: %d\n", error);
        exit(1);
    }
    return 0;
}

static int gralloc_lock_ycbcr(buffer_handle_t handle, uint64_t usage, const struct Rect* bounds, int acquireFence, struct YCbCrLayout* ycbcr)
{
    if(the_gralloc->module->module_api_version < 0x0100)
    {
        fprintf(stderr, "gralloc0 is not supported\n");
        exit(1);
    }
    struct gralloc1_device* dev = (void*)the_gralloc;
    int32_t(*get_num_flex_planes)(struct gralloc1_device*, buffer_handle_t, uint32_t*) = dev->getFunction(dev, GRALLOC1_FUNCTION_GET_NUM_FLEX_PLANES);
    if(!get_num_flex_planes)
    {
        fprintf(stderr, "failed to resolve get_num_flex_planes\n");
        exit(1);
    }
    uint32_t nplanes;
    int error = get_num_flex_planes(dev, handle, &nplanes);
    if(error)
    {
        fprintf(stderr, "get_num_flex_planes failed: %d\n", error);
        exit(1);
    }
    int32_t(*lock_flex)(struct gralloc1_device*, buffer_handle_t, uint64_t, uint64_t, struct gralloc1_rect*, struct android_flex_layout*, int32_t) = dev->getFunction(dev, GRALLOC1_FUNCTION_LOCK_FLEX);
    if(!lock_flex)
    {
        fprintf(stderr, "failed to resolve lock_flex\n");
        exit(1);
    }
    struct android_flex_plane* planes = malloc(sizeof(*planes) * nplanes);
    struct android_flex_layout layout = {
        .planes = planes,
    };
    int ans = lock_flex(dev, handle, usage, usage, (void*)bounds, &layout, acquireFence);
    if(ans)
    {
        fprintf(stderr, "lock_flex failed: %d\n", ans);
        exit(1);
    }
    struct android_flex_plane* y = get_flex_plane(planes, layout.num_planes, FLEX_COMPONENT_Y);
    struct android_flex_plane* cb = get_flex_plane(planes, layout.num_planes, FLEX_COMPONENT_Cb);
    struct android_flex_plane* cr = get_flex_plane(planes, layout.num_planes, FLEX_COMPONENT_Cr);
    if(!y || !cb || !cr)
    {
        fprintf(stderr, "failed to find YCbCr planes in the output\n");
        exit(1);
    }
    ycbcr->y = y->top_left;
    ycbcr->cb = cb->top_left;
    ycbcr->cr = cr->top_left;
    ycbcr->yStride = y->v_increment;
    ycbcr->cStride = cb->v_increment;
    ycbcr->chromaStep = cb->h_increment;
    free(planes);
    return 0;
}

static int gralloc_unlock(buffer_handle_t handle)
{
    if(the_gralloc->module->module_api_version < 0x0100)
    {
        fprintf(stderr, "gralloc0 is not supported\n");
        exit(1);
    }
    struct gralloc1_device* dev = (void*)the_gralloc;
    int32_t(*unlock)(struct gralloc1_device*, buffer_handle_t, int32_t*) = dev->getFunction(dev, GRALLOC1_FUNCTION_UNLOCK);
    if(!unlock)
    {
        fprintf(stderr, "warning: failed to resolve unlock\n");
        return -ENOSYS;
    }
    int32_t sync_fence;
    int error = unlock(dev, handle, &sync_fence);
    if(error)
    {
        fprintf(stderr, "warning: failed to unlock buffer\n");
        return error;
    }
    if(sync_fence >= 0)
    {
        struct pollfd fd = {
            .fd = sync_fence,
            .events = POLLIN,
        };
        while(poll(&fd, 1, -1) != 1);
    }
    return 0;
}

static struct hw_device_t* open_gralloc(const char* soc)
{
    struct hw_module_t* module = open_module("gralloc", soc);
    struct hw_device_t* gralloc;
    int error = module->methods->open(module, "gralloc", &gralloc);
    if(error)
    {
        fprintf(stderr, "Failed to open gralloc device: %d\n", error);
        exit(1);
    }
    void(*set_lock_unlock_ycbcr)(void*, void*, void*) = android_dlsym((void*)-1, "set_lock_unlock_ycbcr");
    if(set_lock_unlock_ycbcr)
        set_lock_unlock_ycbcr(gralloc_lock_simple, gralloc_lock_ycbcr, gralloc_unlock);
    if(!the_gralloc)
        the_gralloc = gralloc;
    return gralloc;
}

static buffer_handle_t gralloc_alloc_blob(const struct hw_device_t* gralloc, size_t width, size_t height, int format)
{
    if(gralloc->module->module_api_version >= 0x100) //gralloc1
    {
        struct gralloc1_device* dev = (void*)gralloc;
        gralloc1_buffer_descriptor_t descr;
        int32_t(*create_descriptor)(struct gralloc1_device*, gralloc1_buffer_descriptor_t*) = dev->getFunction(dev, GRALLOC1_FUNCTION_CREATE_DESCRIPTOR);
        if(!create_descriptor)
        {
            fprintf(stderr, "Failed to resolve create_descriptor\n");
            exit(1);
        }
        int error = create_descriptor(dev, &descr);
        if(error)
        {
            fprintf(stderr, "Failed to create buffer descriptor: %d\n", error);
            exit(1);
        }
        int32_t(*set_consumer_usage)(struct gralloc1_device*, gralloc1_buffer_descriptor_t, uint64_t) = dev->getFunction(dev, GRALLOC1_FUNCTION_SET_CONSUMER_USAGE);
        if(!set_consumer_usage)
        {
            fprintf(stderr, "Failed to resolve set_consumer_usage\n");
            exit(1);
        }
        error = set_consumer_usage(dev, descr, GRALLOC1_CONSUMER_USAGE_CPU_READ_OFTEN);
        if(error)
        {
            fprintf(stderr, "Failed to set consumer usage: %d\n", error);
            exit(1);
        }
        int32_t(*set_dimensions)(struct gralloc1_device*, gralloc1_buffer_descriptor_t, uint32_t, uint32_t) = dev->getFunction(dev, GRALLOC1_FUNCTION_SET_DIMENSIONS);
        if(!set_dimensions)
        {
            fprintf(stderr, "Failed to resolve set_dimensions\n");
            exit(1);
        }
        error = set_dimensions(dev, descr, width, height);
        if(error)
        {
            fprintf(stderr, "Failed to set dimensions: %d\n", error);
            exit(1);
        }
        int32_t(*set_format)(struct gralloc1_device*, gralloc1_buffer_descriptor_t, int32_t) = dev->getFunction(dev, GRALLOC1_FUNCTION_SET_FORMAT);
        if(!set_format)
        {
            fprintf(stderr, "Failed to resolve set_format\n");
            exit(1);
        }
        error = set_format(dev, descr, format);
        if(error)
        {
            fprintf(stderr, "Failed to set format: %d\n", error);
            exit(1);
        }
        int32_t(*set_producer_usage)(struct gralloc1_device*, gralloc1_buffer_descriptor_t, uint64_t) = dev->getFunction(dev, GRALLOC1_FUNCTION_SET_PRODUCER_USAGE);
        if(!set_producer_usage)
        {
            fprintf(stderr, "Failed to resolve set_producer_usage\n");
            exit(1);
        }
        error = set_producer_usage(dev, descr, GRALLOC1_PRODUCER_USAGE_CAMERA);
        if(error)
        {
            fprintf(stderr, "Failed to set producer usage: %d\n", error);
            exit(1);
        }
        int32_t(*allocate)(struct gralloc1_device*, uint32_t, const gralloc1_buffer_descriptor_t*, buffer_handle_t*) = dev->getFunction(dev, GRALLOC1_FUNCTION_ALLOCATE);
        if(!allocate)
        {
            fprintf(stderr, "Failed to resolve allocate\n");
            exit(1);
        }
        buffer_handle_t ans;
        error = allocate(dev, 1, &descr, &ans);
        if(error)
        {
            fprintf(stderr, "Failed to allocate gralloc buffer: %d\n", error);
            exit(1);
        }
        int32_t(*destroy_descriptor)(struct gralloc1_device*, gralloc1_buffer_descriptor_t) = dev->getFunction(dev, GRALLOC1_FUNCTION_DESTROY_DESCRIPTOR);
        if(!destroy_descriptor)
        {
            fprintf(stderr, "warning: failed to resolve destroy_descriptor\n");
            return ans;
        }
        error = destroy_descriptor(dev, descr);
        if(error)
            fprintf(stderr, "warning: failed to destroy buffer descriptor: %d\n", error);
        //workaround for mali gralloc driver
        int(*mali_gralloc_ion_map)(buffer_handle_t) = android_dlsym((void*)-1, "_Z20mali_gralloc_ion_mapP16private_handle_t");
        if(mali_gralloc_ion_map)
        {
            error = mali_gralloc_ion_map(ans);
            if(error)
                fprintf(stderr, "warning: mali_gralloc_ion_map failed\n");
        }
        return ans;
    }
    else
    {
        struct alloc_device_t* dev = (void*)gralloc;
        buffer_handle_t ans;
        int stride = 0;
        int error = dev->alloc(dev, width, height, format, GRALLOC_USAGE_SW_READ_OFTEN | GRALLOC_USAGE_SW_WRITE_NEVER | GRALLOC_USAGE_HW_CAMERA_WRITE, &ans, &stride);
        if(error)
        {
            fprintf(stderr, "gralloc->alloc failed: %d\n", error);
            exit(1);
        }
        return ans;
    }
}

static void gralloc_free_blob(struct hw_device_t* gralloc, buffer_handle_t buf)
{
    if(gralloc->module->module_api_version >= 0x0100) //gralloc1
    {
        struct gralloc1_device* dev = (void*)gralloc;
        int32_t(*release)(struct gralloc1_device*, buffer_handle_t) = dev->getFunction(dev, GRALLOC1_FUNCTION_RELEASE);
        if(!release)
        {
            fprintf(stderr, "warning: failed to resolve release\n");
            return;
        }
        int error = release(dev, buf);
        if(error)
            fprintf(stderr, "warning: buffer release failed: %d\n", error);
    }
    else
    {
        struct alloc_device_t* dev = (void*)gralloc;
        int error = dev->free(dev, buf);
        if(error)
            fprintf(stderr, "warning: gralloc->free failed: %d\n", error);
    }
}

static void on_camera_device_status_change(const struct camera_module_callbacks* arg, int camera_id, int new_status)
{
    printf("camera_device_status_change called, camera_id = %d, new_status = %d\n", camera_id, new_status);
}

static void on_torch_mode_status_change(const struct camera_module_callbacks* arg, const char* camera_id, int new_status)
{
    printf("torch_mode_status_change called, camera_id = %s, new_status = %d\n", camera_id, new_status);
}

struct camera_module_callbacks cb = {on_camera_device_status_change, on_torch_mode_status_change};

static struct camera_module* open_hal(const char* soc)
{
    return (void*)open_module("camera", soc);
}

static size_t(*get_camera_metadata_entry_count)(const struct camera_metadata_t*);
static int(*get_camera_metadata_entry)(const struct camera_metadata_t*, size_t, struct camera_metadata_entry*);
static const char*(*get_camera_metadata_section_name)(uint32_t tag);
static const char*(*get_camera_metadata_tag_name)(uint32_t tag);

static void resolve_camera_metadata(void)
{
    static void* handle;
    if(handle)
        return;
    handle = android_dlopen("libcamera_metadata.so", 2);
    if(!handle)
    {
        fprintf(stderr, "Failed to open libcamera_metadata\n");
        exit(1);
    }
    get_camera_metadata_entry_count = android_dlsym(handle, "get_camera_metadata_entry_count");
    if(!get_camera_metadata_entry_count)
    {
        fprintf(stderr, "Failed to resolve get_camera_metadata_entry_count\n");
        exit(1);
    }
    get_camera_metadata_entry = android_dlsym(handle, "get_camera_metadata_entry");
    if(!get_camera_metadata_entry)
    {
        fprintf(stderr, "Failed to resolve get_camera_metadata_entry\n");
        exit(1);
    }
    get_camera_metadata_section_name = android_dlsym(handle, "get_camera_metadata_section_name");
    if(!get_camera_metadata_section_name)
    {
        fprintf(stderr, "Failed to resolve get_camera_metadata_section_name\n");
        exit(1);
    }
    get_camera_metadata_tag_name = android_dlsym(handle, "get_camera_metadata_tag_name");
    if(!get_camera_metadata_tag_name)
    {
        fprintf(stderr, "Failed to resolve get_camera_metadata_tag_name\n");
        exit(1);
    }
}

static void dump_camera_metadata(const struct camera_metadata_t* meta)
{
    resolve_camera_metadata();
    size_t entry_count = get_camera_metadata_entry_count(meta);
    for(size_t i = 0; i < entry_count; i++)
    {
        struct camera_metadata_entry entry;
        get_camera_metadata_entry(meta, i, &entry);
        const char* section = get_camera_metadata_section_name(entry.tag);
        const char* tag = get_camera_metadata_tag_name(entry.tag);
        if(entry.type >= TYPE_RATIONAL)
            printf("* tag = 0x%" PRIx32 " (%s.%s), unknown type %" PRIu8 ", size %zu\n", entry.tag, section, tag, entry.type, entry.count);
        else
        {
            printf("* tag = 0x%" PRIx32 " (%s.%s), type %" PRIu8 ":", entry.tag, section, tag, entry.type);
            if(entry.type == TYPE_BYTE)
            {
                for(size_t i = 0; i < entry.count; i++)
                    printf(" 0x%" PRIx8, entry.data.u8[i]);
            }
            else if(entry.type == TYPE_INT32)
            {
                for(size_t i = 0; i < entry.count; i++)
                    printf(" %" PRId32, entry.data.i32[i]);
            }
            else if(entry.type == TYPE_FLOAT)
            {
                for(size_t i = 0; i < entry.count; i++)
                    printf(" %f", entry.data.f[i]);
            }
            else if(entry.type == TYPE_INT64)
            {
                for(size_t i = 0; i < entry.count; i++)
                    printf(" %" PRId64, entry.data.i64[i]);
            }
            else if(entry.type == TYPE_DOUBLE)
            {
                for(size_t i = 0; i < entry.count; i++)
                    printf(" %g", entry.data.d[i]);
            }
            printf("\n");
        }
    }
}

struct camera3_notify_msg
{
    int type;
    union
    {
        struct camera3_error_msg
        {
            uint32_t frame_number;
            struct camera3_stream* error_stream;
            int error_code;
        } error;
        struct camera3_shutter_msg
        {
            uint32_t frame_number;
            uint32_t timestamp;
        } shutter;
    };
};

enum
{
    CAMERA3_MSG_ERROR = 1,
    CAMERA3_MSG_SHUTTER = 2,
};

static void on_notify(const struct camera3_callback_ops* self, const struct camera3_notify_msg* msg)
{
    if(msg->type == CAMERA3_MSG_ERROR)
        printf("notify called: error %d on frame number %" PRIu32 "\n", msg->error.error_code, msg->error.frame_number);
    else if(msg->type == CAMERA3_MSG_SHUTTER)
        printf("notify called: shutter time %" PRIu32 " on frame number %" PRIu32 "\n", msg->shutter.frame_number, msg->shutter.timestamp);
    else
        printf("notify called: unknown type %d\n", msg->type);
}

struct camera3_capture_result
{
    uint32_t frame_number;
    const struct camera_metadata_t* result;
    uint32_t num_output_buffers;
    const struct camera3_stream_buffer* output_buffers;
    const struct camera3_stream_buffer* input_buffer;
    uint32_t partial_result;
};

static int result_wait_pipe[2];
static char* output_path = NULL;

static void on_process_capture_result(const struct camera3_callback_ops* self, const struct camera3_capture_result* msg)
{
    printf("process_capture_result called: frame_number = %" PRIu32", num_output_buffers = %" PRIu32 "\n", msg->frame_number, msg->num_output_buffers);
    if(msg->num_output_buffers >= 1)
        printf("output buffer: %p, status=%d\n", msg->output_buffers[0].buffer, msg->output_buffers[0].status);
    if(msg->result)
        dump_camera_metadata(msg->result);
    int frame_nr = msg->frame_number;
    write(result_wait_pipe[1], &frame_nr, sizeof(frame_nr));
}

struct camera3_callback_ops cb2 = {on_process_capture_result, on_notify};

static struct camera3_device* open_camera(const char* path, int which, int init)
{
    char name[16] = {0};
    snprintf(name, 15, "%d", which);
    struct camera_module* mod = open_hal(path);
    if(which < 0 || which >= mod->get_number_of_cameras())
        printf("warning: camera index %d out of range\n", which);
    if(mod->common.module_api_version >= 0x0202)
        mod->set_callbacks(&cb);
    struct hw_device_t* dev;
    int error = mod->common.methods->open((void*)mod, name, &dev);
    if(error)
    {
        fprintf(stderr, "Failed to open camera: %d\n", error);
        exit(1);
    }
    if(dev->tag != HARDWARE_DEVICE_TAG)
    {
        fprintf(stderr, "Invalid device tag 0x%x\n", dev->tag);
        exit(1);
    }
    if(dev->version < 0x0300)
    {
        fprintf(stderr, "Invalid camera version: %x.%02x, expected 3.xx\n", dev->version>>8, dev->version&255);
        exit(1);
    }
    struct camera3_device* ans = (void*)dev;
    if(init)
    {
        error = ans->ops->initialize(ans, &cb2);
        if(error)
        {
            fprintf(stderr, "Failed to initialize camera: %d\n", error);
            exit(1);
        }
    }
    return ans;
}

static void close_camera(struct camera3_device* cam)
{
    int error = cam->common.close((void*)cam);
    if(error)
        printf("warning: failed to close camera: %d\n", error);
}

static void close_gralloc(struct hw_device_t* gralloc)
{
    if(the_gralloc == gralloc)
        the_gralloc = NULL;
    int error = gralloc->close(gralloc);
    if(error)
        printf("warning: failed to close gralloc: %d\n", error);
}

static int checked_atoi(const char* s)
{
    char* endp;
    int ans = strtol(s, &endp, 10);
    if(*endp)
    {
        fprintf(stderr, "Invalid number %s\n", s);
        exit(1);
    }
    return ans;
}

static void parse_size(const char* s, int* width, int* height)
{
    char* endp;
    *width = strtol(s, &endp, 10);
    if(*endp == 'x')
    {
        *height = strtol(endp+1, &endp, 10);
        if(!*endp)
            return;
    }
    fprintf(stderr, "Invalid size %s, expected WIDTHxHEIGHT\n", s);
    exit(1);
}

static void* get_from_camera_metadata(const struct camera_metadata_t* meta, const char* name)
{
    resolve_camera_metadata();
    size_t entry_count = get_camera_metadata_entry_count(meta);
    for(size_t i = 0; i < entry_count; i++)
    {
        struct camera_metadata_entry entry;
        get_camera_metadata_entry(meta, i, &entry);
        const char* section = get_camera_metadata_section_name(entry.tag);
        const char* tag = get_camera_metadata_tag_name(entry.tag);
        if(!section || !tag)
            continue;
        size_t l = strlen(section);
        if(!strncmp(name, section, l) && name[l] == '.' && !strcmp(name+l+1, tag))
            return entry.data.u8;
    }
    fprintf(stderr, "%s not found in camera metadata\n", name);
    exit(1);
}

int writeall(int fd, const void* buf, size_t sz)
{
    const char* p = buf;
    const char* end = buf + sz;
    while(p < end)
    {
        ssize_t chk = write(fd, p, end-p);
        if(chk <= 0)
            return -1;
        p += chk;
    }
    return 0;
}

int save_y4m(int fd, buffer_handle_t buf, int width, int height)
{
    char header[128];
    int chars = sprintf(header, "YUV4MPEG2 W%d H%d F30:1 C420jpeg\nFRAME\n", width, height);
    if(writeall(fd, header, chars))
        return -1;
    struct Rect rect = {0, 0, width, height};
    struct YCbCrLayout layout;
    gralloc_lock_ycbcr(buf, GRALLOC1_CONSUMER_USAGE_CPU_READ_OFTEN, &rect, -1, &layout);
    char* mem = malloc(width*height);
    for(int y = 0; y < height; y++)
        for(int x = 0; x < width; x++)
            mem[y*width+x] = layout.y[y*layout.yStride+x];
    if(writeall(fd, mem, width*height))
    {
        free(mem);
        gralloc_unlock(buf);
        return -1;
    }
    for(int y = 0; y < height/2; y++)
        for(int x = 0; x < width/2; x++)
            mem[y*(width/2)+x] = layout.cb[y*layout.cStride+x*layout.chromaStep];// + 35;
    if(writeall(fd, mem, (width/2)*(height/2)))
    {
        free(mem);
        gralloc_unlock(buf);
        return -1;
    }
    for(int y = 0; y < height/2; y++)
        for(int x = 0; x < width/2; x++)
            mem[y*(width/2)+x] = layout.cr[y*layout.cStride+x*layout.chromaStep];// + 35;
    if(writeall(fd, mem, (width/2)*(height/2)))
    {
        free(mem);
        gralloc_unlock(buf);
        return -1;
    }
    free(mem);
    gralloc_unlock(buf);
    return 0;
}

struct bmp_header
{
    struct
    {
        uint16_t bfType;
        uint32_t bfSize;
        uint8_t reserved[4];
        uint32_t bfOffBits;
    } __attribute__((packed)) fileheader;
    struct
    {
        uint32_t bcSize;
        uint16_t bcWidth;
        uint16_t bcHeight;
        uint16_t bcPlanes;
        uint16_t bcBitCount;
    } __attribute__((packed)) infoheader;
} __attribute__((packed));

int save_converted_bmp(int fd, buffer_handle_t buf, int width, int height)
{
    struct bmp_header header = {
        .fileheader = {
            .bfType = 0x4d42,
            .bfSize = sizeof(header) + 3 * width * height,
            .bfOffBits = sizeof(header),
        },
        .infoheader = {
            .bcSize = sizeof(header.infoheader),
            .bcWidth = width,
            .bcHeight = height,
            .bcPlanes = 1,
            .bcBitCount = 24,
        },
    };
    if(writeall(fd, &header, sizeof(header)))
        return -1;
    struct Rect rect = {0, 0, width, height};
    struct YCbCrLayout layout;
    gralloc_lock_ycbcr(buf, GRALLOC1_CONSUMER_USAGE_CPU_READ_OFTEN, &rect, -1, &layout);
    char* mem = malloc(3*width*height);
    char* p = mem;
    for(int i = 0; i < height; i++)
        for(int j = width - 1; j >= 0; j--)
        {
            int y = layout.y[i*layout.yStride+j];
            int cb = layout.cb[(i/2)*layout.cStride+(j/2)*layout.chromaStep];
            int cr = layout.cr[(i/2)*layout.cStride+(j/2)*layout.chromaStep];
            int r = y + cr * 1436 / 1024 - 179;
            int g = y - cb * 46549 / 131072 + 44 - cr * 93604 / 131072 + 91;
            int b = y + cb * 1814 / 1024 - 227;
            if(r < 0)
                r = 0;
            if(r >= 256)
                r = 255;
            if(g < 0)
                g = 0;
            if(g >= 256)
                g = 255;
            if(b < 0)
                b = 0;
            if(b >= 256)
                b = 255;
            *p++ = b;
            *p++ = g;
            *p++ = r;
        }
    int ans = writeall(fd, mem, 3*width*height);
    free(mem);
    gralloc_unlock(buf);
    return ans;
}

int save_blob(int fd, buffer_handle_t buf, int size)
{
    void* mapping;
    struct Rect rect = {0, 0, size, 1};
    gralloc_lock_simple(buf, GRALLOC1_CONSUMER_USAGE_CPU_READ_OFTEN, &rect, -1, &mapping);
    struct camera3_jpeg_blob* header = (void*)((char*)mapping + size - sizeof(*header));
    if(header->jpeg_blob_id != CAMERA3_JPEG_BLOB_ID)
    {
        fprintf(stderr, "Error: jpeg_blob_id is invalid\n");
        gralloc_unlock(buf);
        return -1;
    }
    int ans = writeall(fd, mapping, header->jpeg_size);
    gralloc_unlock(buf);
    return ans;
}

void usage(const char* program_name)
{
    printf(R"(usage: %s [options...] <soc> [camera_nr [subcommand [args...]]]

Subcommands:
    %s <soc>                                    Prints generic info about the camera HAL
    %s <soc> <camera_nr>                        Prints generic info about the specific camera device
    %s <soc> <camera_nr> open                   Attempts to open (and immediately close) the camera device
    %s <soc> <camera_nr> dump                   Calls the dump method on the camera device, might dump some internal debugging state
    %s <soc> <camera_nr> dump_settings          Dumps the default capture settings of the camera device
    %s <soc> <camera_nr> capture <output_file>  Attempts to take a picture and save it to <output_file>

Options:
    --wait <seconds>         Wait the specified number of seconds after opening the camera. Some HALs return early from the initialize call.
    --error-wait <seconds>   Wait the specified number of seconds before exiting in case of errors. Useful if the HAL is printing its error messages in another thread.
    --size <width>x<height>  Set the frame size to request from the HAL. Default is 640x480.
    --no-close               Do not try to close the camera device upon exit.
    --frames <N>             Take N frames, save the last one. Some HALs return garbage in the first few frames.
    --frame-cap <N>          Limit the request parallelism to N. Default is min(10, frames).
    --jpg                    Request a JPEG image. The output format will be JPEG. By default, Y4M with YUV420 is used.
    --to-bmp                 Convert YUV420 to RGB, and save the result as BMP. The output format will be BMP.

)", program_name, program_name, program_name, program_name, program_name, program_name, program_name);
    exit(0);
}

int main(int argc, const char** argv)
{
    int wait_time = 0;
    int error_wait_time = 0;
    int width = 640;
    int height = 480;
    int format = 0;
    int to_bmp = 0;
    int nframes = 1;
    int no_close = 0;
    int frame_cap = 0;
    int argc_new = !!argc;
    for(int i = 1; i < argc; i++)
    {
        if(!strcmp(argv[i], "--wait"))
            wait_time = checked_atoi(argv[++i]);
        else if(!strcmp(argv[i], "--error-wait"))
            error_wait_time = checked_atoi(argv[++i]);
        else if(!strcmp(argv[i], "--frames"))
            nframes = checked_atoi(argv[++i]);
        else if(!strcmp(argv[i], "--frame-cap"))
            frame_cap = checked_atoi(argv[++i]);
        else if(!strcmp(argv[i], "--no-close"))
            no_close = 1;
        else if(!strcmp(argv[i], "--size"))
            parse_size(argv[++i], &width, &height);
        else if(!format && !strcmp(argv[i], "--jpg"))
            format = HAL_PIXEL_FORMAT_BLOB;
        else if(!format && !strcmp(argv[i], "--to-bmp"))
        {
            format = HAL_PIXEL_FORMAT_YCBCR_420_888;
            to_bmp = 1;
        }
        else
            argv[argc_new++] = argv[i];
    }
    argc = argc_new;
    if(argc == 2)
    {
        struct camera_module* mod = open_hal(argv[1]);
        printf("Module API version: %x.%02x\n", mod->common.module_api_version >> 8, mod->common.module_api_version & 255);
        printf("HAL API version: %x.%02x\n", mod->common.hal_api_version >> 8, mod->common.hal_api_version & 255);
        printf("ID: %s\n", mod->common.id);
        printf("Name: %s\n", mod->common.name);
        printf("Author: %s\n", mod->common.author);
        printf("Number of cameras: %d\n", mod->get_number_of_cameras());
    }
    else if(argc == 3)
    {
        int which = checked_atoi(argv[2]);
        struct camera_module* mod = open_hal(argv[1]);
        struct camera_info info;
        if(which < 0 || which >= mod->get_number_of_cameras())
            printf("warning: camera index %d out of range\n", which);
        int error = mod->get_camera_info(which, &info);
        if(error)
        {
            fprintf(stderr, "Error getting camera info: %d\n", error);
            return 1;
        }
        printf("Facing: %d\n", info.facing);
        printf("Orientation: %d\n", info.orientation);
        printf("Device version: %x.%02x\n", info.device_version >> 8, info.device_version & 255);
        if(info.device_version >= 0x200)
        {
            printf("Resource cost: %d\n", info.resource_cost);
            if(info.conflicting_devices_length)
            {
                printf("Conflicting devices:");
                for(size_t i = 0; i < info.conflicting_devices_length; i++)
                    printf(" %s", info.conflicting_devices[i]);
                printf("\n");
            }
        }
        dump_camera_metadata(info.static_camera_characteristics);
    }
    else if(argc == 4 && !strcmp(argv[3], "open"))
    {
        struct camera3_device* cam = open_camera(argv[1], checked_atoi(argv[2]), 0);
        sleep(wait_time);
        if(!no_close)
            close_camera(cam);
    }
    else if(argc == 4 && !strcmp(argv[3], "dump"))
    {
        struct camera3_device* cam = open_camera(argv[1], checked_atoi(argv[2]), 1);
        sleep(wait_time);
        printf("Device version: %x.%02x\n", cam->common.version>>8, cam->common.version&255);
        printf("-- BEGIN DUMP --\n");
        fflush(stdout);
        cam->ops->dump(cam, 1);
        printf("-- END DUMP --\n");
        fflush(stdout);
        if(!no_close)
            close_camera(cam);
    }
    else if(argc == 4 && !strcmp(argv[3], "dump_settings"))
    {
        struct camera3_device* cam = open_camera(argv[1], checked_atoi(argv[2]), 1);
        sleep(wait_time);
        const struct camera_metadata_t* settings = cam->ops->construct_default_request_settings(cam, 0);
        dump_camera_metadata(settings);
        if(!no_close)
            close_camera(cam);
    }
    else if(argc == 5 && !strcmp(argv[3], "capture"))
    {
        if(pipe(result_wait_pipe))
        {
            perror("pipe failed\n");
            exit(1);
        }
        struct hw_device_t* gralloc = open_gralloc(argv[1]);
        struct camera_module* mod = open_hal(argv[1]);
        struct camera3_device* cam = open_camera(argv[1], checked_atoi(argv[2]), 1);
        sleep(wait_time);
        struct camera_info info;
        int error = mod->get_camera_info(checked_atoi(argv[2]), &info);
        if(error)
        {
            fprintf(stderr, "Error getting camera info: %d\n", error);
            sleep(error_wait_time);
            return 1;
        }
        size_t jpeg_size = *(int32_t*)get_from_camera_metadata(info.static_camera_characteristics, "android.jpeg.maxSize");
        printf("android.jpeg.maxSize = %zu\n", jpeg_size);
        const struct camera_metadata_t* settings = cam->ops->construct_default_request_settings(cam, 0);
        int blob_width = width;
        int blob_height = height;
        if(format == HAL_PIXEL_FORMAT_BLOB)
        {
            blob_width = jpeg_size + sizeof(struct camera3_jpeg_blob);
            blob_height = 1;
        }
        else if(!format)
            format = HAL_PIXEL_FORMAT_YCBCR_420_888;
        buffer_handle_t* buf = malloc(sizeof(*buf) * nframes);
        for(int i = 0; i < nframes; i++)
            buf[i] = gralloc_alloc_blob(gralloc, blob_width, blob_height, format);
        if(frame_cap == 0)
        {
            frame_cap = nframes;
            if(frame_cap > 10)
                frame_cap = 10;
        }
        struct camera3_stream stream = {
            .stream_type = CAMERA3_STREAM_OUTPUT,
            .width = width,
            .height = height,
            .format = format,
            .max_buffers = frame_cap,
            .data_space = 0,
            .rotation = 0,
        };
        struct camera3_stream* stream_ptr = &stream;
        struct camera3_stream_configuration stream_cfg = {
            .num_streams = 1,
            .streams = &stream_ptr,
        };
        error = cam->ops->configure_streams(cam, &stream_cfg);
        if(error)
        {
            sleep(error_wait_time);
            fprintf(stderr, "Failed to configure streams: %d\n", error);
            return 1;
        }
        struct camera3_stream_buffer* stream_buf = calloc(sizeof(*stream_buf), nframes);
        for(int i = 0; i < nframes; i++)
        {
            stream_buf[i].stream = &stream;
            stream_buf[i].buffer = buf + i;
            stream_buf[i].status = CAMERA3_BUFFER_STATUS_OK;
            stream_buf[i].acquire_fence = -1;
            stream_buf[i].release_fence = -1;
        }
        struct camera3_capture_request* requests = calloc(sizeof(*requests), nframes);
        int waited_for = 0;
        for(int i = 0; i < nframes; i++)
        {
            requests[i].frame_number = i+1;
            requests[i].settings = settings;
            requests[i].input_buffer = NULL;
            requests[i].num_output_buffers = 1;
            requests[i].output_buffers = stream_buf+i;
            requests[i].num_physical_settings = 0;
            int error = cam->ops->process_capture_request(cam, requests+i);
            if(error)
            {
                fprintf(stderr, "Error on frame %d: process_capture_request returned %d\n", i+1, error);
                return 1;
            }
            int high = (i == nframes - 1) ? nframes : i - frame_cap + 2;
            for(; waited_for < high; waited_for++)
            {
                int frame_nr;
                read(result_wait_pipe[0], &frame_nr, sizeof(frame_nr));
                if(stream_buf[frame_nr-1].release_fence >= 0)
                {
                    struct pollfd fd = {
                        .fd = stream_buf[frame_nr-1].release_fence,
                        .events = POLLIN,
                    };
                    while(poll(&fd, 1, -1) != 1);
                }
            }
        }
        int output_fd = open(argv[4], O_WRONLY | O_CREAT | O_TRUNC, 0666);
        if(output_fd < 0)
            fprintf(stderr, "Failed to open output file %s: %s\n", argv[4], strerror(errno));
        else
        {
            int status;
            if(format == HAL_PIXEL_FORMAT_BLOB)
                status = save_blob(output_fd, buf[nframes-1], blob_width);
            else if(to_bmp)
                status = save_converted_bmp(output_fd, buf[nframes-1], width, height);
            else
                status = save_y4m(output_fd, buf[nframes-1], width, height);
            if(status)
                perror("Failed to save output file");
            close(output_fd);
        }
        for(int i = 0; i < nframes; i++)
            gralloc_free_blob(gralloc, buf[i]);
        if(!no_close)
            close_camera(cam);
        close_gralloc(gralloc);
        free(requests);
        free(stream_buf);
        free(buf);
    }
    else
        usage(argv[0]);
    return 0;
}
