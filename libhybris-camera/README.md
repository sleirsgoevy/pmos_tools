# libhybris-camera

A "small" script to open the camera HAL via libhybris and capture images.

There are no dependencies, except libc and the "android\_dlopen/android\_dlsym" symbols from libhybris-common.

# Building

Just run `make`.

# Example usage

```
./main <soc> 0 capture --to-bmp /tmp/capture.bmp
```

See `./main --help` for more options. `<soc>` the SoC codename (e.g. `exynos7870`).

# Environment

Android's `/system` and `/vendor` partitions must be mounted under the correct paths. Device files must be present in `/dev` and accessible to the process.

# fake\_gralloc\_wrapper.so

Samsung's camera driver attempts to access the gralloc via `/dev/hwbinder`. This functionality lives in `/vendor/lib/libGrallocWrapper.so`.

Since I don't want to start any actual binder services for this PoC, I replaced this library with my own implementation that does not use binder.
