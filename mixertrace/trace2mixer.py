import sys, os

controls = {int(s.split('=', 1)[1].split(',', 1)[0]): 'name='+s.split(',name=', 1)[1].strip() for s in os.popen('amixer controls') if s.startswith('numid=')}

f = open(sys.argv[1])
for line in f:
    if line.startswith('Thread '):
        first = next(f)
        if first.count('{') == 1 and first.count('}') == 1 and first.count(',') == 2:
            a, b, c = [int(i, 16) for i in first.split('{', 1)[1].split('}', 1)[0].split(',')]
            second = next(f)
            if second.startswith("'"):
                path = second.strip()[1:-1]
                if path.startswith('/dev/snd/'):
                    third = next(f)
                    if third.count('{') == 1 and third.count('}') == 1:
                        bb = bytes(int(i, 16) for i in third.split('{', 1)[1].split('}', 1)[0].split(','))
                        if b == 0xc2c85513: # SNDRV_CTL_IOCTL_ELEM_WRITE
                            index = int.from_bytes(bb[:4], 'little')
                            value = int.from_bytes(bb[72:76], 'little')
                            print('amixer -D sysdefault cset', controls[index], value)
