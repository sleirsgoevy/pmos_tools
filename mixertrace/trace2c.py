import sys

print("#include <sys/ioctl.h>\n#include <fcntl.h>\n#include <string.h>\n\nint main()\n{\n    char buf[4096];")

path2file = {}

f = open(sys.argv[1])
for line in f:
    if line.startswith('Thread '):
        first = next(f)
        if first.count('{') == 1 and first.count('}') == 1 and first.count(',') == 2:
            a, b, c = [int(i, 16) for i in first.split('{', 1)[1].split('}', 1)[0].split(',')]
            second = next(f)
            if second.startswith("'"):
                path = second.strip()[1:-1]
                if path.startswith('/dev/snd/'):
                    if path not in path2file:
                        path2file[path] = 'fd' + str(len(path2file))
                        print('int', path2file[path], '= open("'+path+'", O_RDWR);')
                    third = next(f)
                    if third.count('{') == 1 and third.count('}') == 1:
                        bb = bytes(int(i, 16) for i in third.split('{', 1)[1].split('}', 1)[0].split(','))
                        print('    memcpy(buf, "'+''.join('\\x%02x'%i for i in bb)+'", %d);'%len(bb))
                        print('    ioctl(%s, 0x%x, buf);'%(path2file[path], b))
                    else:
                        print('    ioctl(%s, 0x%x, 0x%x);'%(path2file[path], b, c))

print('}')
