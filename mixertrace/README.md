# mixertrace

This tool can be used for **automatic** porting of ALSA-based sound cards. Probably only useful for downstream-kernel ports.

The instruction below assumes that you have 32-bit Android installed on the phone and sound works there.

## Capturing

Steps:

1. Boot into Android.
2. Put the phone into silent mode and reboot. This is to ensure that the sound card is initialized as late as possible.
3. Use `fuser /dev/snd/*` command to determine audioserver's PID.
4. Chroot into a Linux chroot that has gdb and python3 installed. Don't forget to mount at least /proc.
5. Type the command `gdb --pid PID_OF_AUDIOSERVER_SEE_ABOVE -x /path/to/mixertrace.gdbinit` to start the capture.
6. Make the phone put its soundcard into the desired state, e.g. dial a number or launch a video at YouTube.
7. Once you hear the sound playing, exit gdb by pressing Ctrl-C Ctrl-D Ctrl-D.

The debugger sometimes stops on a breakpoint. If that happens, just type `cont` to let it continue. If that happens more than once, you can just press Enter to continue.

Note that the default output file is `/root/output.log`. Edit the script if you want another path.

## trace2c.py

`python3 trace2c.py /root/output.log > /root/replay.c`

(or wherever you want to place the replay source)

This tool generates a C source from the recorded trace. Make sure to compile it as 32-bit, as ioctl numbers differ.

## trace2mixer.py

`python3 trace2mixer.py /root/output.log > /root/amixer.sh`

(or wherever you want to place the replay script)

This tool generates a shell script that replays the mixer configuration being performed using the amixer command.

## Write a UCM file

(This is what I did, you may do it the other way if you know how.)

1. Run `strace -f -e trace=file alsaucm set _verb HiFi`, and see which path it tries to access.
2. Copy `/usr/share/alsa/ucm2/Samsung/snow/snow.conf` to match that path, and `/usr/share/alsa/ucm2/Samsung/snow/HiFi.conf` to the same directory.
3. Remove all existing cset directives from the conf files.
4. Rewrite the activation script into the main config's SectionDefaults as cset commands. Make sure to enclose the arguments in double quotes.

Note: if your sound card breaks on alsactl restore (mine did), add this into your `/etc/init.d/alsa`:

```
restore() {
    ebegin "Restoring Mixer Levels"

    alsaucm set _verb HiFi              # add this
```

This ensures that the required initialization runs before attempting to restore mixer levels.
